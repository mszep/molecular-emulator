from molemu import (InterParticleForce,
                    VelocityVerletIntegrator,
                    IdealSpringInteraction)

from nmigen.back.pysim import Simulator, Tick, Settle


def force(p):
    return (10000 - p)//8


def velocityverlet_python(p0, v0, a0, nsteps):

    p = p0
    v = v0
    a = a0
    m = 16
    f = 0  # force(p)
    yield (p, v, a, f)
    for i in range(nsteps - 1):
        p_new = p + v + a // 2
        f_new = 0  # force(p_new)
        a_new = f_new // m
        v_new = v + (a + a_new) // 2
        p = p_new
        f = f_new
        a = a_new
        v = v_new
        yield (p, v, a, f)


def test_velocityverletintegrator(nsteps=100):

    # args = {'p0': 800, 'v0': 0, 'f0': 200, 'nsteps': 10}

    i = VelocityVerletIntegrator(num_bits=64, ghost_layer=32)

    def testbench():

        py_results = velocityverlet_python(0, 0, 0, nsteps)

        yield i.rst.eq(1)
        yield Tick()
        yield i.rst.eq(0)
        for _ in range(nsteps):
            yield i.f.eq(0)
            yield Tick()
            res_nmigen = ((yield i.p), (yield i.v), (yield i.a), (yield i.f))
            res_python = next(py_results)

            # Dont test f because there is some ambiguity in
            # how we will pipe it in
            assert res_nmigen[0] == res_python[0]
            assert res_nmigen[1] == res_python[1]
            assert res_nmigen[2] == res_python[2]

            # print('p {:10d} v {:10d} a {:10d} f {:10d} '.format(*res_nmigen))

    sim = Simulator(i)
    sim.add_clock(1e-6)
    sim.add_sync_process(testbench)

    sim.run()


def test_idealspringinteraction():

    args = {'p1': -4, 'p2': 2, 'p0': 4, 'kinv': 20}

    def ideal_spring(p1, p2, p0, kinv):
        return ((p2 - p1) - p0) // kinv

    block = IdealSpringInteraction(num_bits=64,
                                   p0=args['p0'],
                                   kinv=args['kinv'])

    sim = Simulator(block)

    def process():

        res_python = ideal_spring(**args)

        for key in ['p1', 'p2']:
            yield block.__getattribute__(key).eq(args[key])

        yield Settle()

        res_nmigen = (yield block.f)

        assert res_python == res_nmigen

    sim.add_process(process)

    sim.run()


def test_interparticleforce():

    args = {'x1': 12, 'y1': 0, 'z1': 0,
            'x2':  2, 'y2': 0, 'z2': 0,
            'epsilon': 1, 'sigma': 20}

    def lennard_jones(x1, y1, z1, x2, y2, z2, epsilon, sigma):
        r2 = (x2 - x1)**2 + (y2 - y1)**2 + (z2 - z1)**2
        prefactor = (24 *
                     epsilon *
                     (2 * (sigma**12 // r2**7) - (sigma**6 // r2**4)))
        return [prefactor * delta for delta in [x2 - x1, y2 - y1, z2 - z1]]

    block = InterParticleForce(num_bits=64)

    sim = Simulator(block)

    def process():

        # Get the desired result
        res_python = lennard_jones(**args)

        # Set the arguments to the nmigen module
        for key, val in args.items():
            yield block.__getattribute__(key).eq(val)

        # Not 100% clear why we need to Settle, but doesn't work without this
        yield Settle()

        res_nmigen = [(yield block.res[0]),
                      (yield block.res[1]),
                      (yield block.res[2])]

        assert res_nmigen == res_python

    sim.add_process(process)

    sim.run()

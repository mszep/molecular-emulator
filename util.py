import matplotlib.pyplot as plt
from parsec import string, many, none_of, sepBy, regex, generate, \
                   space, spaces


def twos_comp(val, bits):
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0:  # if sign bit is set,
        val = val - (1 << bits)         # compute negative value
    return val                          # return positive value as is


# Parse a comment in a VCD file
comment = string('$comment') >> many(none_of('\n')) << string('\n')

# Parse a date in a VCD file
date = string('$date') >> many(none_of('\n')) << string('\n')

# Parse a timescale specification in a VCD file
timescale = string('$timescale') >> many(none_of('\n')) << string('\n')

# Split a string into words, terminated by a newline
words = sepBy(regex('[^\n ]*'), string(' '))


@generate
def wire_definition():
    '''Parse a VCD wire definition, e.g. "$var wire 8 2e v0 $end\n"
    '''
    defn = yield string('$var wire') >> space() >> words << string('\n')
    # Need to return result as a list of tuples so that the result is the
    # same type as an entire module (see below).
    return [{'width': defn[0],
             'identifier': defn[1],
             'reference': defn[2]}]


@generate
def module():
    '''Parse a VCD module definition, e.g.
    $scope module foomodule $end\n
    [wire definitions]
    $upscope $end\n'''

    module_start = string('$scope module') >> space() >> words << string('\n')
    module_end = string('$upscope') << many(none_of('\n')) << string('\n')

    module_name = (yield module_start)[0]
    elements = yield many(wire_definition ^ module)
    yield module_end

    # `elements` is a list of lists of tuples, so flatten the list and prepend
    # the first elements with the current scope's module name.
    return [{'reference': module_name + '-' + d['reference'],
             'identifier': d['identifier'],
             'width': d['width']} for el in elements for d in el]


@generate
def simulation_time():
    '''Parse a VCD simulation time statement, e.g. ""#15000\n"
    '''
    s = yield string('#') >> regex('[0-9]*') << string('\n')
    return [('simulation_time', s)]


@generate
def scalar_value_change():
    '''Parse a VCD scalar value change, e.g. "02e\n"
    '''
    s = yield regex(r'[01].*\s')
    return [('scalar_value_change', s)]


@generate
def vector_value_change():
    '''Parse a VCD vector value change, e.g. "b0011011 2e\n".
    '''
    s = yield regex(r'b[01]+\s.*\s')
    return [('vector_value_change', s)]


@generate
def dumpvars():
    '''Parse a VCD dumpvars statement, where current values of variables
    are printed as a group.'''
    dumpvars_start = string('$dumpvars')
    yield dumpvars_start
    yield spaces()
    elements = yield many(scalar_value_change ^ vector_value_change)
    yield string('$end') >> spaces()
    return [el for sublist in elements for el in sublist]


@generate
def vcd_parser():
    '''Parse an entire VCD file, returning `names`, the mapping of
    identifiers to references, `widths`, the width in bits of each
    signal, and a list of all simulation commands.'''

    # Ignore comment, date and timescale statements
    yield many(comment ^ date ^ timescale)

    # Parse the top level module, and all submodules into `wire_defs`.
    wire_defs = yield module

    # strip off the 'top-' prefix for top module variables
    for wd in wire_defs:
        wd['reference'] = wd['reference'].split('-', maxsplit=1)[1]

    names = {wd['identifier']: wd['reference'] for wd in wire_defs}
    widths = {wd['reference']: int(wd['width']) for wd in wire_defs}

    yield string('$enddefinitions $end\n')

    simulation_commands = yield many(simulation_time ^
                                     dumpvars ^
                                     scalar_value_change ^
                                     vector_value_change)

    commands_list = [sc for sublist in simulation_commands for sc in sublist]

    return names, widths, commands_list


def get_traces(names, widths, simulation_commands):
    '''Walk the list of simulation commands and reconstruct the time series
    of each signal.'''

    # Initialize the traces dict
    traces = {name: {'timestamp': [], 'value': []} for name in names.values()}

    # Initialize the time to -1 just in case just in case the first
    # simulation command is not a timestamp
    time = -1

    # Run through all the simulation commands
    for cmd_type, cmd_val in simulation_commands:
        if cmd_type == 'simulation_time':
            time = int(cmd_val)
        elif cmd_type == 'scalar_value_change':
            newval = int(cmd_val[0])
            identifier = cmd_val[1:].strip()
            name = names[identifier]
            traces[name]['timestamp'].append(time)
            traces[name]['value'].append(newval)
        elif cmd_type == 'vector_value_change':
            words = cmd_val.split()
            binary_string = words[0].replace('b', '')
            identifier = words[1]
            newval = twos_comp(int(binary_string, 2),
                               widths[names[identifier]])
            name = names[identifier]
            traces[name]['timestamp'].append(time)
            traces[name]['value'].append(newval)
        else:
            # Should not happen
            raise ValueError("Unrecognized command type {}".format(cmd_type))

    return traces


def plot_traces(traces, signames=None):
    '''Plot the traces of a set of signals.
    '''
    if signames is None:
        # Plot all traces
        signames = list(traces.keys())
    elif type(signames) == str:
        # Plot only a single named trace
        signames = [signames]

    plt.figure()
    for n in signames:
        plt.step(traces[n]['timestamp'], traces[n]['value'],
                 where='post', label=n)
    plt.legend()

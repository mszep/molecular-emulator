from nmigen import Module, Signal, Elaboratable, Array
from nmigen import signed


class VelocityVerletIntegrator(Elaboratable):
    def __init__(self, num_bits, ghost_layer):
        self.p0 = Signal(signed(num_bits))  # position
        self.v0 = Signal(signed(num_bits))  # velocity
        self.a0 = Signal(signed(num_bits))  # acceleration
        self.f = Signal(signed(num_bits))  # [total] force
        self.m = 16                 # mass
        self.rst = Signal(1)

        self.num_bits = num_bits
        self.ghost_layer = ghost_layer
        self.box_limit = 2 ** (self.num_bits - 1) - self.ghost_layer

        self.p = Signal(signed(self.num_bits))  # position
        self.v = Signal(signed(self.num_bits))  # velocity
        self.a = Signal(signed(self.num_bits))  # acceleration

    def elaborate(self, platform: str):
        m = Module()

        with m.If(self.rst == 1):
            m.d.sync += self.p.eq(self.p0)
            m.d.sync += self.v.eq(self.v0)
            m.d.sync += self.a.eq(self.a0)
        with m.Else():
            p_new = self.p + self.v + self.a // 2
            f_new = self.f
            with m.If(p_new > self.box_limit):
                # Particle is hitting the top of the box, bounce it back
                p_new_bounced = self.box_limit - (p_new - self.box_limit)
                a_new = f_new // self.m
                v_new = -(self.v + (self.a + a_new) // 2)
                m.d.sync += [self.p.eq(p_new_bounced)]
                m.d.sync += [self.a.eq(a_new)]
                m.d.sync += [self.v.eq(v_new)]
            with m.Elif(p_new < -self.box_limit):
                # Particle is hitting the bottom of the box, bounce it back
                p_new_bounced = -self.box_limit + (-self.box_limit - p_new)
                a_new = f_new // self.m
                v_new = -(self.v + (self.a + a_new) // 2)
                m.d.sync += [self.p.eq(p_new_bounced)]
                m.d.sync += [self.a.eq(a_new)]
                m.d.sync += [self.v.eq(v_new)]
            with m.Else():
                a_new = f_new // self.m
                v_new = self.v + (self.a + a_new) // 2
                m.d.sync += [self.p.eq(p_new)]
                m.d.sync += [self.a.eq(a_new)]
                m.d.sync += [self.v.eq(v_new)]

        return m


class IdealSpringInteraction(Elaboratable):
    def __init__(self, p0, kinv, num_bits):
        '''Args:
        `p0`: equilibrium distance
        `kinv`: inverse spring constant
        '''
        self.p1 = Signal(signed(num_bits))
        self.p2 = Signal(signed(num_bits))
        self.f = Signal(signed(num_bits))
        self.p0 = p0
        self.kinv = kinv

    def elaborate(self, platform: str):
        m = Module()
        dp = self.p2 - self.p1
        m.d.comb += self.f.eq((dp - self.p0) // self.kinv)
        return m


class InterParticleForce(Elaboratable):
    def __init__(self, num_bits):
        self.x1 = Signal(num_bits)
        self.y1 = Signal(num_bits)
        self.z1 = Signal(num_bits)
        self.x2 = Signal(num_bits)
        self.y2 = Signal(num_bits)
        self.z2 = Signal(num_bits)
        self.res = Array([Signal(signed(num_bits)) for _ in range(3)])
        self.epsilon = Signal(num_bits)
        self.sigma = Signal(num_bits)

    def elaborate(self, platform: str):
        m = Module()
        with m.If(self.x2 > self.x1):
            dx = (self.x2 - self.x1)
        with m.Else():
            dx = (self.x1 - self.x2)
        with m.If(self.y2 > self.y1):
            dy = (self.y2 - self.y1)
        with m.Else():
            dy = (self.y1 - self.y2)
        with m.If(self.z2 > self.z1):
            dz = (self.z2 - self.z1)
        with m.Else():
            dz = (self.z1 - self.z2)
        dx2 = dx * dx
        dy2 = dy * dy
        dz2 = dz * dz
        r2 = dx2 + dy2 + dz2
        sigma2 = self.sigma * self.sigma
        sigma3 = sigma2 * self.sigma
        sigma6 = sigma3 * sigma3
        sigma12 = sigma6 * sigma6
        r22 = r2 * r2
        r23 = r22 * r2
        r24 = r22 * r22
        r27 = r24 * r23
        prefactor = (24 *
                     self.epsilon *
                     (2 * (sigma12 // r27) - (sigma6 // r24)))
        resx = prefactor * (self.x2 - self.x1)
        resy = prefactor * (self.y2 - self.y1)
        resz = prefactor * (self.z2 - self.z1)
        m.d.comb += [self.res[0].eq(resx)]
        m.d.comb += [self.res[1].eq(resy)]
        m.d.comb += [self.res[2].eq(resz)]
        return m


class Cell(Elaboratable):
    def __init__(self, num_bits, ghost_layer, num_integrators=2):
        self.p0 = Array([Signal(signed(num_bits))
                         for _ in range(num_integrators)])  # position
        self.v0 = Array([Signal(signed(num_bits))
                         for _ in range(num_integrators)])  # velocity
        self.a0 = Array([Signal(signed(num_bits))
                         for _ in range(num_integrators)])  # acceleration
        self.f = Array([Signal(signed(num_bits))
                        for _ in range(num_integrators)])  # [total] force
        self.m = 1                 # mass
        self.rst = Signal(1)

        self.p = Array([Signal(signed(num_bits))
                        for _ in range(num_integrators)])  # position
        self.v = Array([Signal(signed(num_bits))
                        for _ in range(num_integrators)])  # velocity
        self.a = Array([Signal(signed(num_bits))
                        for _ in range(num_integrators)])  # acceleration

        self.integrators = Array(
            [VelocityVerletIntegrator(num_bits, ghost_layer)
             for _ in range(2)])

        self.interactions = Array([IdealSpringInteraction(1800000000,
                                                          90,
                                                          num_bits)])

    def elaborate(self, platform: str):

        m = Module()
        m.submodules += self.integrators
        m.submodules += self.interactions
        for i in range(len(self.integrators)):
            m.d.comb += [self.integrators[i].p0.eq(self.p0[i]),
                         self.integrators[i].v0.eq(self.v0[i]),
                         self.integrators[i].a0.eq(self.a0[i]),
                         self.integrators[i].f.eq(self.f[i]),
                         self.integrators[i].rst.eq(self.rst),
                         self.p[i].eq(self.integrators[i].p),
                         self.v[i].eq(self.integrators[i].v),
                         self.a[i].eq(self.integrators[i].a)]
        m.d.comb += [self.interactions[0].p1.eq(self.p[0]),
                     self.interactions[0].p2.eq(self.p[1]),
                     self.f[0].eq(self.interactions[0].f),
                     self.f[1].eq(-self.interactions[0].f)]
        return m
